# Share Buttons

This repo contains common share buttons that can easily be embedded onto any website.

## Button types
The following share buttons are supported:
* Facebook
* Twitter
* Gab
* Telegram
* Copy page URL button

## Multiple formats
For each share button type, we offer multiple formats for different environments where you may be embedding the buttons:
* Static HTML (works on any site)
* React component

## Styles
There are multiple styles of buttons you can embed on your site, depending on how you want things to look:
* Floating share buttons – large circular buttons that float on the top right corner of the window
* More coming as needed

## Contributions
If there's anything that you think needs to be added, we welcome your additions. File a PR (keep your PRs limited in scope) and there's a good chance we'll add it.

## Example images

### Floating share buttons
![Floating share buttons](./screenshots/floating-share-buttons.png)
