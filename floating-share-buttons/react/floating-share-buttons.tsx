import { Component } from 'react';

/**
 * Types of buttons that can be displayed
 */
export type ButtonType =
    | 'facebook'
    | 'twitter'
    | 'gab'
    | 'telegram'
    | 'copyurl';

/**
 * Properties of the floating share buttons component
 */
export interface FloatingShareButtonsProps {
    buttons: ButtonType[];
    message?: string;
    url?: string;
}

/**
 * Generator functions for share URLs for each platform
 */
const PLATFORM_SHARE_GENS: {[key in ButtonType]?: ((url: string, message: string) => string)} = {
    'facebook': (url, message) => `https://www.facebook.com/sharer/sharer.php?u=${url}&text=${message}`,
    'twitter': (url, message) => `https://twitter.com/intent/tweet?url=${url}&text=${message}`,
    'gab': (url, message) => `https://gab.com/compose?url=${url}&text=${message}`,
    'telegram': (url, message) => `https://t.me/share/url?url=${url}&text=${message}`,
};

export class FloatingShareButtons extends Component<FloatingShareButtonsProps> {

    /**
     * Renders the share buttons component
     */
    public render() {
        return (
            <div className="floating-share-buttons">
                { this.props.buttons.map(type => {
                    return (
                        <div>
                            <button
                                className={ type }
                                onClick={ this.clickedButton.bind(this, type) }>{ type === 'gab' ? 'gab' : '' }</button>
                        </div>
                    );
                }) }
                <div><button className="facebook"></button></div>
                <div><button className="twitter"></button></div>
                <div><button className="gab">gab</button></div>
                <div><button className="telegram"></button></div>
                <div><button className="copyurl"></button></div>
            </div>
        );
    }

    /**
     * Called when a button is clicked
     * @param type the type of button clicked
     */
    private clickedButton(type: ButtonType): void {

        // Handle share platforms
        if (type in PLATFORM_SHARE_GENS) {
            const shareurl: string = PLATFORM_SHARE_GENS[type](
                this.props.url ?? document.location.href,
                this.props.message ?? '',
            );
            window.open(shareurl, '_blank');
            return;
        }

        // If we're copying the url
        if (type === 'copyurl') {
            const input = document.createElement('input');
            document.body.append(input);
            input.value = this.props.url ?? document.location.href;
            input.select();
            input.setSelectionRange(0, 9999999);
            document.execCommand('copy');
            input.remove();
            return;
        }

    }

}